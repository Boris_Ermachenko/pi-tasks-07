#pragma once
#include "ProjectManager.h"
class SeniorManager : public ProjectManager
{
public:
	SeniorManager(int id = 0, string fio = "", string project = "", int quality = 0, double contribution=0, int worktime=0, int payment=0){
		this->id = id;
		this->fio = fio;
		this->project = project;
		this->quality = quality;
		this->worktime = worktime;
		this->payment = payment;
		this->contribution = contribution;
	}

	void SetPayment() {
		this->payment = (EstimationProject(1000, contribution) + 10*EstimationHeading(quality))*0.9;
	}

	~SeniorManager(){}
};

