#pragma once
#include"Project.h"
#include"Engineer.h"
using namespace std;
class Programmer: public Engineer
{
public:
	Programmer();
	Programmer(int id, string fio, int worktime, double payment, int stavka, int projectID, double bonus, int projectMoney): Engineer(id, fio, worktime, payment, stavka, projectID, bonus, projectMoney) {};
	~Programmer();
};

