#pragma once
#include"ProjectManager.h"
class SeniorManager: public ProjectManager
{
private:
	int payForOneProject;
	int numProject;
public:
	SeniorManager();
	SeniorManager(int id, string fio, double payment, int projectID, double include, int projectMoney, int countPeople, int payForOne, int payForOneProject,
		int numProject) : ProjectManager(id, fio, payment, projectID, include, projectMoney, countPeople, payForOne) {
		this->numProject = numProject;
		this->payForOneProject = payForOneProject;
	}
	int payForProjects(int payForOneProject, int numProject);
	void pay();
	~SeniorManager();
};

