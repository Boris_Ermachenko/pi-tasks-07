#pragma once
#include <string>
using namespace std;
class Employee
{
private:
	int id;
	string fio;
	int worktime;
	double payment;
public:
	Employee();
	Employee(int id, string fio,int worktime, double payment);
	void setPay(double pay);
	void setFIO(string fio);
	void setWorktime(int hour);
	void setId(int id);
	int getID();
	string getFIO();
	int getWorktime();
	double getPayment();
	virtual void pay() = 0;
	~Employee();
};

