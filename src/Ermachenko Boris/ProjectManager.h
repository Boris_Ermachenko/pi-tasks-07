#pragma once
#include"Heading.h"
#include"Manager.h"
using namespace std;
class ProjectManager:public Heading, public Manager
{
protected:
	int countPeople;
	int payForOne;
public:

	ProjectManager();
	ProjectManager(int id, string fio, double payment, int projectID, double include, int projectMoney, int countPeople, int payForOne) : Manager(id, fio, payment, projectID, include, projectMoney) {
		this->countPeople = countPeople;
		this->payForOne = payForOne;
	}
	int setpayHead(int countPeople, int payForOne);
	void pay();
	~ProjectManager();
};

