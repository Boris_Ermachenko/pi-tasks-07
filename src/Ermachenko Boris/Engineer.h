#pragma once
#include <string>
#include"Employee.h"
#include"Project.h"
#include"WorkTime.h"
using namespace std;
class Engineer: public Employee, public Project, public WorkTime
{
protected:
	int stavka;
	int projectMoney;
	int projectID;
	double bonus;
public:
	Engineer();
	Engineer(int id, string fio, int worktime, double payment, int stavka, int projectID, double bonus, int projectMoney) :Employee(id, fio, worktime, payment) {
		this->stavka = stavka;
		this->projectMoney = projectMoney;
		this->projectID = projectID;
		this->bonus = bonus;
	}
	void setBonus(double bonus);
	void setStavka(int stavka);
	void setprojectID();
	int getStavka();
	double getBonus();
	int setpayWorkTime(int stavka, int hours);
	double setpayProject(int moneyPay, double include);
	int getProjectMoney();
	void pay();
	~Engineer();
};

