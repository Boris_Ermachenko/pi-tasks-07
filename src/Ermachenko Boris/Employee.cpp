#include "Employee.h"
Employee::Employee()
{
}

Employee::Employee(int id, string fio, int worktime, double payment)
{
	this->id = id;
	this->fio = fio;
	this->worktime = worktime;
	this->payment = payment;

}

void Employee::setPay(double pay)
{
	payment = pay;
}

void Employee::setFIO(string fio)
{
	this->fio = fio;
}

void Employee::setWorktime(int hour)
{
	worktime = hour;
}

void Employee::setId(int id)
{
	this->id = id;
}

int Employee::getID()
{
	return id;
}

string Employee::getFIO()
{
	return fio;
}

int Employee::getWorktime()
{
	return worktime;
}

double Employee::getPayment()
{
	return payment;
}



Employee::~Employee()
{
}
