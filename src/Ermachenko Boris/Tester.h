#pragma once
#include"Project.h"
#include"Engineer.h"
class Tester : public Engineer
{
public:
	Tester();
	Tester(int id, string fio, int worktime, double payment, int stavka, int projectID, double bonus, int projectMoney) : Engineer(id, fio, worktime, payment, stavka, projectID, bonus, projectMoney) {};
	~Tester();
};

