#pragma once
#include"Heading.h"
#include"Programmer.h"
class TeamLeader: public Heading, public Programmer
{
	int countPeople;
	int payForOne;
public:
	TeamLeader();
	TeamLeader(int id, string fio, int worktime, double payment, int stavka, int projectID, double bonus, int projectMoney, int countPeople,int payForOne) : Programmer(id, fio, worktime, payment, stavka, projectID, bonus, projectMoney) {
		this->countPeople = countPeople;
		this->payForOne = payForOne;
	};
	int setpayHead(int countPeople, int payForOne);
	void pay();
	~TeamLeader();
};

