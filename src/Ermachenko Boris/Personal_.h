#pragma once
#include"Employee.h"
#include"WorkTime.h"
using namespace std;
class Personal_: public Employee, WorkTime
{
private:
	int stavka;
public:
	Personal_();
	Personal_(int id, string fio, int worktime, double payment, int stavka) : Employee(id, fio, worktime, payment), stavka(stavka) {};
	int setpayWorkTime(int stavka, int hours);
	void pay();
	~Personal_();
};

