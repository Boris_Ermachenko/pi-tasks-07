#pragma once
#include <string>
#include"Employee.h"
#include"Project.h"
#include"WorkTime.h"
using namespace std;
class Manager: public Employee, public Project
{
protected:
	double include;
	int projectMoney;
	int projectID;
public:

	Manager();
	Manager(int id, string fio, double payment, int projectID, double include, int projectMoney) : Employee(id, fio, 0, payment) {
		this->include = include;
		this->projectMoney = projectMoney;
		this->projectID = projectID;
		//this->
	}
	//void setProject(int projectId);
	//void setProjectMoney();
	//void setProjectID();
	//void setInclude();
	double setpayProject(int moneyPay, double include);
	int getProject();
	int getProjectMoney();
	double getInclude();
	void pay();
	~Manager();
};

